package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.EntityLogDTO;

public interface ILoggerService {

    void writeLog(@NotNull EntityLogDTO message);
}

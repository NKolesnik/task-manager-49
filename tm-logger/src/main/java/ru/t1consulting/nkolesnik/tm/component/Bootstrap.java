package ru.t1consulting.nkolesnik.tm.component;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.service.IReceiverService;
import ru.t1consulting.nkolesnik.tm.listener.LoggerListener;
import ru.t1consulting.nkolesnik.tm.service.ReceiverService;

public class Bootstrap {

    public void run() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
                ActiveMQConnection.DEFAULT_BROKER_URL
        );
        factory.setUserName("admin");
        factory.setPassword("admin");
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

}

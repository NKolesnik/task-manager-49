package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.service.ILoggerService;
import ru.t1consulting.nkolesnik.tm.dto.EntityLogDTO;

import java.io.File;
import java.io.FileOutputStream;

public class LoggerService implements ILoggerService {

    @NotNull
    public static final String PROJECTS_LOG_FILE_NAME = "./logger/projects.log";

    @NotNull
    public static final String TASKS_LOG_FILE_NAME = "./logger/tasks.log";

    @NotNull
    public static final String USERS_LOG_FILE_NAME = "./logger/users.log";

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final EntityLogDTO message) {
        @Nullable final String className = message.getClassName();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        @NotNull final String header = "Id: " + message.getId() +
                "; Type: " + message.getType() +
                "; Date: " + message.getDate() +
                "\n";
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntity().getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Nullable
    private String getFileName(@NotNull final String className) {
        switch (className) {
            case "Project":
            case "ProjectDTO":
                return PROJECTS_LOG_FILE_NAME;
            case "Task":
            case "TaskDTO":
                return TASKS_LOG_FILE_NAME;
            case "User":
            case "UserDTO":
                return USERS_LOG_FILE_NAME;
            default:
                return null;
        }
    }

}

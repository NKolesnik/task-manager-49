package ru.t1consulting.nkolesnik.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.component.MessageExecutor;
import ru.t1consulting.nkolesnik.tm.enumerated.EntityOperationType;

import javax.persistence.*;

import static ru.t1consulting.nkolesnik.tm.enumerated.EntityOperationType.*;

@NoArgsConstructor
public class EntityListener {

    @NotNull
    private static final MessageExecutor MESSAGE_EXECUTOR = new MessageExecutor();

    @SneakyThrows
    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        MESSAGE_EXECUTOR.sendMessage(entity, operationType.toString());
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity) {
        sendMessage(entity, PRE_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        sendMessage(entity, POST_PERSIST);
    }

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        sendMessage(entity, POST_LOAD);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity) {
        sendMessage(entity, PRE_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, POST_UPDATE);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity) {
        sendMessage(entity, PRE_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        sendMessage(entity, POST_REMOVE);
    }

}


package ru.t1consulting.nkolesnik.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.service.ISenderService;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import ru.t1consulting.nkolesnik.tm.dto.EntityLogDTO;


import javax.jms.*;
import java.util.Date;

public class SenderService implements ISenderService {

    @NotNull
    private static final String TOPIC_NAME = "TM_TOPIC";

    @NotNull
    private final ActiveMQConnectionFactory factory;

    public SenderService() {
        factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setUserName("admin");
        factory.setPassword("admin");
    }

    @Override
    @SneakyThrows
    public void send(@NotNull final EntityLogDTO entity) {
        @NotNull final Connection connection = factory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(TOPIC_NAME);
        final MessageProducer producer = session.createProducer(destination);
        final ObjectMessage message = session.createObjectMessage(entity);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

    @Override
    @SneakyThrows
    @NotNull
    public EntityLogDTO createMessage(@NotNull final Object object, @NotNull final String type) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        @NotNull final String className = object.getClass().getSimpleName();
        @NotNull final EntityLogDTO message = new EntityLogDTO(className, new Date().toString(), json, type);
        return message;
    }

}


package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.EntityLogDTO;

public interface ISenderService {

    void send(@NotNull EntityLogDTO entity);

    @NotNull
    EntityLogDTO createMessage(@NotNull Object object, @NotNull String type);

}
